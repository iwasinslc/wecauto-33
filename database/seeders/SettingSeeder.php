<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class SettingSeeder
 */
class SettingSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $settings = [
            'site-on'       => 'on',
            'support-email' => 'admin@hyipium.com',
            'phone'         => '',
            'email'         => '',
            'telegram'      => '',
            'whatsapp'      => '',
            'company_name'  => '',
            'address'       => '',
            'working_time'  => '',
            'online_chat'   => '',
            'price_up_amount'=>'',
            'price_down_amount_for_percent'=>3000,
            'usdt_to_usd'=>1,
            'auto_limit' => 100,
            'moto_limit' => 100
        ];

        foreach ($settings as $settingKey => $settingValue) {
            $checkExists = DB::table('settings')->where('s_key', $settingKey)->count();

            if ($checkExists > 0) {
                echo "Setting '".$settingKey."' already registered.\n";
                continue;
            }

            DB::table('settings')->insert([
                's_key' => $settingKey,
                's_value' => $settingValue,
                'created_at' => now()
            ]);
            echo "Setting '".$settingKey."' registered.\n";
        }
    }
}
