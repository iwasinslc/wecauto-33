<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RequestCashbackRequestUpdate;
use App\Models\CashbackRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CashbackResultController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('profile.cashback_result.index');
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function datatable(Request $request)
    {
        $cashbackRequests = CashbackRequest::query()
            ->with(['user'])
            ->when($request->filled('date_from'), function ($query) use ($request) {
                return $query->where('created_at', '>=', Carbon::createFromFormat('m/d/Y', request()->date_from )->toDateTimeString());
            })
            ->when($request->filled('date_to'), function ($query) use ($request) {
                return $query->where('created_at', '<=', Carbon::createFromFormat('m/d/Y', request()->date_to )->toDateTimeString());
            })
            ->get();

        return DataTables::of($cashbackRequests)
            ->addColumn('show', function ($data) {
                return route('profile.cashback-result.show', ['id' => $data->id]);
            })
            ->addColumn('status', function($data) {
                return $data->approved === null
                    ? 'На рассмотрении'
                    : ($data->approved ? 'Одобрена' : 'Отклонена');
            })
            ->make(true);
    }

    /**
     * @param string $cashbackRequestId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show(string $cashbackRequestId)
    {
        /** @var CashbackRequest $cashbackRequest */
        $cashbackRequest = CashbackRequest::query()
            ->with(['user', 'documents'])
            ->find($cashbackRequestId);

        if (!$cashbackRequest) {
            return redirect(route('profile.profile'))->with('error', 'Cashback request not found');
        }

        return view('profile.cashback_result.show', [
            'cashbackRequest' => $cashbackRequest,
            'user' => $cashbackRequest->user,
            'documents' => $cashbackRequest->getMedia('cashback_request')
        ]);
    }

    /**
     * Update cashback request
     * @param RequestCashbackRequestUpdate $request
     * @param $cashbackRequestId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function update(RequestCashbackRequestUpdate $request, $cashbackRequestId)
    {
        /** @var CashbackRequest $cashbackRequest */
        $cashbackRequest = CashbackRequest::findOrFail($cashbackRequestId);

        $update = [];
        if ($request->filled('documents_checked')) {
            $update['documents_checked'] = $request->documents_checked == 1;
        }
        if ($request->filled('video_checked')) {
            $update['video_checked'] = $request->video_checked == 1;
        }
        if ($request->filled('approved')) {
            if ($request->approved) {
                try {
                    // Approve cashback request
                    $cashbackRequest->update([
                        'approved' => true,
                        'result' => $request->result
                    ]);
                    return back()->with('success', __('Cashback request approved'));
                } catch (\Exception $e) {
                    return back()->with('error', $e->getMessage())->withInput();
                }
            } else {
                // Reject cashback request
                $update += [
                    'approved' => false,
                    'result' => $request->result
                ];
            }
        }

        if (!empty($update)) {
            $cashbackRequest->update($update);
            return back()->with('success', __('Cashback request updated'));
        } else {
            return back()->with('error', __('Updated data request is empty'));
        }
    }
}
