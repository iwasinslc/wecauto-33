<?php
namespace App\Http\Requests;

use App\Models\PaymentSystem;
use App\Rules\RuleEnoughBalance;
use App\Rules\RuleHasLicence;
use App\Rules\RuleUUIDEqual;
use App\Rules\RuleWalletExist;
use App\Rules\RuleWalletWithExternalAddress;
use App\Rules\RuleWithdrawEnoughBalance;
use App\Rules\RuleWithdrawWalletExist;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestWithdraw
 * @package App\Http\Requests
 *
 * @property string currency_id
 * @property string payment_system_id
 * @property float amount
 * @property string captcha
 * @property  string external
 */
class RequestWithdraw extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fields = [
            'currency' => 'required|max:255|string',
            'amount'    => ['numeric' , new RuleWithdrawWalletExist(), new RuleWithdrawEnoughBalance,  new RuleHasLicence ,'min:0.1', 'max:1000000'],
            'external' => 'required|max:255|string',
        ];


        $extractCurrency = explode(':', request()->currency);

        if (count($extractCurrency) != 2) {
            return back()->with('error', __('Unable to read data from request'))->withInput();
        }

        $paymentSystem = PaymentSystem::where('id', $extractCurrency[1])->first();
        $currency = $paymentSystem->currencies()->where('id', $extractCurrency[0])->first();

        if (
            config('app.env') != 'develop' // Check only for production. Not check for develop
            && isset($paymentSystem->getRegEx()[$currency->code])
        )
        {
            $fields['external'] = 'required|max:255|string|regex:'.$paymentSystem->getRegEx()[$currency->code];
        }


        return $fields;
    }

    /**
     * @return array
     */
    public function messages()
    {
        $fields = [
//            'wallet_id.required' => __('Wallet is required'),
            'amount.numeric'     => __('Amount have to be numeric'),
//
//            'captcha.required'   => trans('validation.captcha_required'),
//            'captcha.captcha'    => trans('validation.captcha_captcha'),
        ];

        $extractCurrency = explode(':', request()->currency);

        if (count($extractCurrency) != 2) {
            return back()->with('error', __('Unable to read data from request'))->withInput();
        }

        $paymentSystem = PaymentSystem::where('id', $extractCurrency[1])->first();
        $currency = $paymentSystem->currencies()->where('id', $extractCurrency[0])->first();

        if (isset($paymentSystem->getRegEx()[$currency->code]))
        {

            $fields['external.regex'] = __('Wallet address format is invalid for').' '.$paymentSystem->name.' '.$currency->code;
        }



        return $fields;
    }
}
