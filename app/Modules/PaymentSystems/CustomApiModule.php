<?php
namespace App\Modules\PaymentSystems;

use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Withdraw;
use Illuminate\Support\Str;


/**
 * Class CustomApiModule
 * @package App\Modules\PaymentSystems
 */
class CustomApiModule
{

    /**
     * @param $address
     * @return mixed
     */
    public static function getServer($address)
    {
        $addresses = [
            'CA'=>'http://crypto-accelerator.io/',
            'WT'=>'https://webtokenprofit.com/',
            'PB'=>'https://your-profitbot.com/'
        ];

        $abb = explode(':', $address)[0];

        return $addresses[$abb];

    }
//    /**
//     * @return array
//     * @throws \Exception
//     */
    public static function getBalances(): array
    {
        $ps       = PaymentSystem::getByCode('custom');
        $balances = [];

        foreach ($ps->currencies as $currency) {
            try {
                $balances[$currency->code] = self::getBalance($currency->code);
            } catch (\Exception $exception) {
                throw new \Exception($exception->getMessage());
            }
        }

        if (count($balances) > 0 && !empty($ps)) {
            $ps->update([
                'external_balances' => json_encode($balances),
                'connected' => true,
            ]);
        } else {
            $ps->update([
                'external_balances' => json_encode([]),
                'connected' => false,
            ]);
            throw new \Exception('Balance is not reachable.');
        }

        return $balances;
    }
    
    public static function transfer(Withdraw $transaction
    ) {






        /** @var Wallet $wallet */
        $wallet         = $transaction->wallet()->first();
        /** @var User $user */
        $user           = $wallet->user()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $transaction->paymentSystem()->first();
        /** @var Currency $currency */
        $currency       = $transaction->currency()->first();

        if (null === $wallet || null === $user || null === $paymentSystem || null === $currency) {
            throw new \Exception('Wallet, user, payment system or currency is not found for withdrawal approve.');
        }

        $data = [
            'address'=>$transaction->source,
            'currency'=>$currency->code,
            'amount'=>$transaction->amount-$transaction->commission
        ];

        $result = self::request(self::getServer($transaction->source),'GET','/custom/status', $data);

        if (isset($result->error)) {
            throw new \Exception('Can not withdraw '.$transaction->amount.$currency->symbol.'. Reason: '.$result->error);
        }



        return $result->result;
    }

    /**
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    public static function getBalance(string $currency)
    {


        return 0;
    }



    public static function getAddress()
    {

        return 'WA:'.Str::random();
    }


    /**
     * @param string $method
     * @param string $address
     * @param array $data
     * @param array|null $additionHeaders
     * @return object
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function request($api_url, $method, $address, $data=[], $additionHeaders=null)
    {
        $client   = new \GuzzleHttp\Client();
        $headers  = [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
        ];
        $params   = [
            'headers' => array_merge($headers, (is_array($additionHeaders) ? $additionHeaders : [])),
            'verify'  => false,
            //'json'    => $data,
        ];

        $fields = http_build_query($data, '', '&');

        try {
            $response = $client->request($method, $api_url.$address.'?api_key='.env('ACC_API_KEY').'&'.$fields, $params);
        } catch (\Exception $e) {
            throw new \Exception('Request to '.$address.' is failed. '.$e->getMessage());
        }

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Request to '.$address.' was with response status '.$response->getStatusCode());
        }

        return json_decode($response->getBody()->getContents());
    }
}