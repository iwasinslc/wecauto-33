<?php
namespace App\Observers;

use App\Models\Projects;

/**
 * Class ProjectsObserver
 * @package App\Observers
 */
class ProjectsObserver
{
    /**
     * @param Projects $project
     */
    public function deleting(Projects $project)
    {
        foreach ($project->likes()->get() as $child) {
            $child->delete();
        }
    }

    /**
     * @param Projects $project
     * @return array
     */
    private function getCacheKeys(Projects $project): array
    {
        return [];
    }

    /**
     * @param Projects $project
     * @return array
     */
    private function getCacheTags(Projects $project): array
    {
        return [];
    }

    /**
     * Listen to the Projects created event.
     *
     * @param Projects $project
     * @return void
     * @throws
     */
    public function created(Projects $project)
    {
        clearCacheByArray($this->getCacheKeys($project));
        clearCacheByTags($this->getCacheTags($project));
    }

    /**
     * Listen to the Projects deleting event.
     *
     * @param Projects $project
     * @return void
     * @throws
     */
    public function deleted(Projects $project)
    {
        clearCacheByArray($this->getCacheKeys($project));
        clearCacheByTags($this->getCacheTags($project));
    }

    /**
     * Listen to the Projects updating event.
     *
     * @param Projects $project
     * @return void
     * @throws
     */
    public function updated(Projects $project)
    {
        clearCacheByArray($this->getCacheKeys($project));
        clearCacheByTags($this->getCacheTags($project));
    }
}