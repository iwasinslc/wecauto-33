<?php


namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="User",
 *     description="User Model",
 *     @OA\Xml(
 *         name="User"
 *     )
 * )
 */
class User
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     type="string",
     *     example="070d9b20-06d7-11eb-93ee-951cd25dc5ca"
     * )
     *
     * @var string
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Name",
     *     description="Full name of the user",
     *     type="string"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     title="Email",
     *     description="Email of the user",
     *     example="example@email.com",
     *     type="string"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     title="Email Verified At",
     *     description="Date of the email verification",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $email_verified_at;

    /**
     * @OA\Property(
     *     title="Login",
     *     description="Login of the user",
     *     example="login123",
     *     type="string"
     * )
     *
     * @var string
     */
    public $login;

    /**
     * @OA\Property(
     *     title="Phone",
     *     description="Phone of the user",
     *     example="380661111111",
     *     type="string",
     *     minLength=10,
     *     maxLength=20
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     title="Skype",
     *     description="Skype address of the user",
     *     example="live: skype",
     *     type="string",
     *     maxLength=255
     * )
     *
     * @var string
     */
    public $skype;

    /**
     * @OA\Property(
     *     title="Google 2FA",
     *     description="Flag which show status of google 2 factor authorization",
     *     example=true,
     *     type="boolean"
     * )
     *
     * @var boolean
     */
    public $google_2fa;

    /**
     * @OA\Property(
     *     title="Referral Link",
     *     description="Refferal link of the user",
     *     example="http://www.domain.com/path/id",
     *     type="string"
     * )
     *
     * @var string
     */
    public $referral_link;

    /**
     * @OA\Property(
     *     title="Rank",
     *     description="Rane Name for the user",
     *     example="Partner",
     *     type="string"
     * )
     *
     * @var string
     */
    public $rank;

    /**
     * @OA\Property(
     *     title="Country",
     *     description="Country of the user",
     *     example="Ukraine",
     *     type="string"
     * )
     *
     * @var string
     */
    public $country;

    /**
     * @OA\Property(
     *     title="Avatar",
     *     description="Link to user avatar",
     *     example="https://domain/path/img.jpg",
     *     type="string"
     * )
     *
     * @var string
     */
    public $avatar;

    /**
     * @OA\Property(
     *     title="Registration date",
     *     description="Date when user was created",
     *     example="2020-01-01 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $registration_data;
}