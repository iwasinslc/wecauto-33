<?php
namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AuthAttempts
 * @package App\Models
 *
 * @property string ip
 * @property integer attempts
 * @property integer blocked
 */
class AuthAttempts extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var string $table */
    protected $table = 'auth_attempts';
    protected $keyType = 'string';

    /** @var array $fillable */
    protected $fillable = [
        'ip',
        'attempts',
        'blocked',
    ];

    /**
     * @return bool
     */
    public function isBlocked()
    {
        return $this->blocked == 1;
    }
}
