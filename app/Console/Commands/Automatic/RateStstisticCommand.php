<?php
namespace App\Console\Commands\Automatic;

use App\Jobs\AccrueDeposit;
use App\Jobs\CloseDeposit;
use App\Models\Currency;
use App\Models\DepositQueue;
use App\Models\PriceUps;
use App\Models\RateStatistic;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Console\Command;

/**
 * Class DepositQueueCommand
 * @package App\Console\Commands\Automatic
 */
class RateStstisticCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rate:statistic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deposits queues.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle()
    {
        $start_time = now()->subHours(3)->toDateTimeString();
        $end_time = now()->toDateTimeString();
        $types = [
            TransactionType::getByName('exchange_buy')->id,
            TransactionType::getByName('exchange_sell')->id,
        ];

        $transactions = Transaction::whereIn('type_id', $types)->where('currency_id', Currency::getByCode('USD')->id)->where('created_at','>=' ,$start_time)->where('created_at','<=' ,$end_time)->get();

        if ($transactions->count()==0)
        {
           die();
        }


        $stat = new RateStatistic();

        $open = Transaction::whereIn('type_id', $types)->where('currency_id', Currency::getByCode('USD')->id)->where('created_at','>=' ,$start_time)->where('created_at','<=' ,$end_time)->orderBy('created_at')->first();
        $close = Transaction::whereIn('type_id', $types)->where('currency_id', Currency::getByCode('USD')->id)->where('created_at','>=' ,$start_time)->where('created_at','<=' ,$end_time)->orderBy('created_at', 'desc')->first();

        $stat->open = $open->source;
        $stat->close = $close->source;
        $stat->low = $transactions->min('source');
        $stat->high = $transactions->max('source');
        $stat->created_at = now()->toDateTimeString();
        $stat->save();




    }
}
