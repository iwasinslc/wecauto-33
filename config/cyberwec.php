<?php

return [
    'token'  => env('CYBERWEC_TOKEN'),
    'source' => env('CYBERWEC_SOURCE'),
];
