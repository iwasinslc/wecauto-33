@extends('layouts.auth')
@section('title', __('Register'))

@section('content')



    <div class="auth__content">
        <div class="auth-module auth-module--not-content">
            <form class="auth-module__form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <h3 class="auth-module__title">{{__('Register')}}
                </h3>
                <!-- .field--error-->
                <div class="field field--row">
                    <label>{{ __('E-Mail Address') }}</label>
                    <input class="field-stroke js-input-symbols-limit" type="email" onkeyup="return forceLower(this);" name="email"
                           value="{{ old('email') }}" required >
                </div>

                <div class="field field--row">
                    <label>{{ __('Login name') }}</label>
                    <input class="field-stroke" type="text" onkeyup="return forceLower(this);" name="login"
                           value="{{ old('login') }}" >
                </div>


                <div class="field field--row">
                    <label>{{ __('Telegram or Phone number') }}</label>
                    <input class="field-stroke js-input-symbols-limit" type="text" name="phone"
                           value="{{ old('phone') }}" >
                </div>


                <div class="field field--row">
                    <label>{{ __('Partner ID') }}</label>
                    <input class="field-stroke" type="text"  class="form-control"
                           value="{{ !empty(getPartnerInfoFromCookies()) ? getPartnerInfoFromCookies()['login'] : '' }}"
                           disabled>
                    <input type="hidden" name="partner_id" value="{{ !empty(getPartnerInfoFromCookies()) ? getPartnerInfoFromCookies()['my_id'] : '' }}">
                </div>

{{--                <div class="field field--row">--}}
{{--                    <label>{{ __('Payment Code') }}</label>--}}
{{--                    <input class="field-stroke" type="number" step="1" min="6" name="payment_code"--}}
{{--                           value="{{ old('payment_code') }}" >--}}
{{--                </div>--}}


                <div class="field field--row">
                    <label>{{ __('Password') }}</label>
                    <input class="field-stroke" type="password" name="password"
                           required >
                </div>

                <div class="field field--row">
                    <label>{{ __('Confirm Password') }}</label>
                    <input class="field-stroke" type="password" name="password_confirmation"
                           required >
                </div>

                <div class="field field--row">
                    <label>{{ __('Enter captcha code') }}</label>
                    <div class="field__group">
                        <input class="field-stroke" type="text" name="captcha" id="captcha">
                        <div class="captcha" style="cursor: pointer"  onclick="refreshCaptcha()"><?= captcha_img() ?>
                        </div>
                    </div>
                </div>


                <div class="field">
                    <label class="checkbox">
                        <input type="checkbox" id="agreement" name="agreement" value="1" checked><span>{{ __('I agree with') }}</span><a href="{{route('customer.agreement')}}" target="_blank">{{ __('Terms of Service & Privacy Policy')}}</a>
                    </label>
                </div>
                <div class="auth-module__buttons">
                    <button class="btn btn--warning btn--size-lg">{{__('Register')}}
                    </button>
                    <ul>
                        <li><a href="{{route('login')}}">{{__('Sign in')}}</a></li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('load-scripts')
    <script>
        function forceLower(strInput)
        {
            strInput.value=strInput.value.toLowerCase();
        }
    </script>
@endpush
